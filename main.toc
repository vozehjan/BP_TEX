\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {czech}
\defcounter {refsection}{0}\relax 
\select@language {czech}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{vi}{section*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Re\IeC {\v s}er\IeC {\v s}n\IeC {\'\i } \IeC {\v c}\IeC {\'a}st}{3}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Sou\IeC {\v c}asn\IeC {\'y} stav}{3}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}V\IeC {\'y}b\IeC {\v e}r voliteln\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{3}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Tvorba \IeC {\'u}vazk\IeC {\r u}}{5}{subsection.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Existuj\IeC {\'\i }c\IeC {\'\i } \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{5}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Bakal\IeC {\'a}\IeC {\v r}i}{7}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}\IeC {\v S}kola OnLine}{8}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Anal\IeC {\'y}za a~n\IeC {\'a}vrh}{9}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Funk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{9}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Vyps\IeC {\'a}n\IeC {\'\i } nab\IeC {\'\i }dky voliten\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{9}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Volba voliteln\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} studenty}{10}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Vyhodnocen\IeC {\'\i } z\IeC {\'a}pisu voliteln\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{10}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Evidence p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} a~aprobac\IeC {\'\i }}{11}{subsection.2.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.5}Sb\IeC {\v e}r po\IeC {\v z}adavk\IeC {\r u} od u\IeC {\v c}itel\IeC {\r u}}{11}{subsection.2.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.6}Tvorba \IeC {\'u}vazk\IeC {\r u}}{11}{subsection.2.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.7}Test dat}{11}{subsection.2.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.8}Export dat}{12}{subsection.2.1.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Nefunk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{13}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Webov\IeC {\'a} aplikace}{13}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Technologie}{13}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}U\IeC {\v z}ivatelsk\IeC {\'e} role}{14}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}P\IeC {\v r}\IeC {\'\i }pady u\IeC {\v z}it\IeC {\'\i }}{15}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Z\IeC {\'a}pis voliteln\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} \ref {img:uc:zapis}}{15}{subsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.1}Spr\IeC {\'a}va z\IeC {\'a}pis\IeC {\r u} \ref {img:uc:sprava_zapisu}}{16}{subsubsection.2.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.2}V\IeC {\'y}b\IeC {\v e}r p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} \ref {img:uc:vyber_predmetu}}{17}{subsubsection.2.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.3}Vyhodnocen\IeC {\'\i } z\IeC {\'a}pisu \ref {img:uc:vyhodnoceni}}{18}{subsubsection.2.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Tvorba \IeC {\'u}vazk\IeC {\r u} \ref {img:uc:uvazky}}{20}{subsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Spr\IeC {\'a}va p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} \ref {img:uc:sprava_predmetu}}{21}{subsection.2.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.4}Export dat \ref {img:uc:export_dat}}{22}{subsection.2.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Datab\IeC {\'a}zov\IeC {\'e} sch\IeC {\'e}ma}{24}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}N\IeC {\'a}vrh grafick\IeC {\'e}ho u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{30}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}P\IeC {\v r}ihl\IeC {\'a}\IeC {\v s}en\IeC {\'\i } studenta pro v\IeC {\'y}b\IeC {\v e}r volitel\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{30}{subsection.2.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}P\IeC {\v r}ehled z\IeC {\'a}pis\IeC {\r u} studenta}{30}{subsection.2.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.3}Vypln\IeC {\v e}n\IeC {\'\i } formul\IeC {\'a}\IeC {\v r}e z\IeC {\'a}pisu -- v\IeC {\'y}b\IeC {\v e}r p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{33}{subsection.2.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.4}Detail z\IeC {\'a}pisu a~odpov\IeC {\v e}di studenta}{34}{subsection.2.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.5}Detail z\IeC {\'a}pisu s~v\IeC {\'y}sledky}{34}{subsection.2.6.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.6}Seznam p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u}}{35}{subsection.2.6.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.7}Detail p\IeC {\v r}edm\IeC {\v e}tu}{36}{subsection.2.6.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.8}Seznam aprobac\IeC {\'\i }}{36}{subsection.2.6.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.9}Seznam z\IeC {\'a}pis\IeC {\r u}}{36}{subsection.2.6.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.10}Detail z\IeC {\'a}pisu}{38}{subsection.2.6.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.11}Formul\IeC {\'a}\IeC {\v r} pro vyhodnocen\IeC {\'\i } z\IeC {\'a}pisu}{38}{subsection.2.6.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.12}Formul\IeC {\'a}\IeC {\v r} pro p\IeC {\v r}i\IeC {\v r}azen\IeC {\'\i } \IeC {\'u}vazk\IeC {\r u}}{40}{subsection.2.6.12}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}Implementace}{41}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Pou\IeC {\v z}it\IeC {\'e} technologie}{41}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Framework Symfony}{42}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.1}Architektura MVC}{42}{subsubsection.3.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.2}Konzole}{43}{subsubsection.3.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.3}Bundly}{43}{subsubsection.3.1.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.4}Zpracov\IeC {\'a}n\IeC {\'\i } a~sm\IeC {\v e}rov\IeC {\'a}n\IeC {\'\i } po\IeC {\v z}adavku}{44}{subsubsection.3.1.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.5}Adres\IeC {\'a}\IeC {\v r}ov\IeC {\'a} struktura}{45}{subsubsection.3.1.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}HTML}{46}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}CSS}{46}{subsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.4}PHP}{46}{subsection.3.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.5}JavaScript}{47}{subsection.3.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.6}jQuery}{47}{subsection.3.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.7}Bootstrap}{47}{subsection.3.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.8}Twig}{47}{subsection.3.1.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.9}Doctrine 2}{48}{subsection.3.1.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.10}MySQL}{48}{subsection.3.1.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.11}Composer}{48}{subsection.3.1.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.12}Pou\IeC {\v z}it\IeC {\'e} bundly}{49}{subsection.3.1.12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Architektura aplikace}{50}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Entity}{50}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Validace dat}{52}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Asociace}{53}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Controllery}{55}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.5}Views}{56}{subsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.6}Repository}{56}{subsection.3.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.7}Formul\IeC {\'a}\IeC {\v r}e}{57}{subsection.3.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.8}Princip Repository, Functionality, Operation}{58}{subsection.3.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.9}Autorizace a~autentizace}{59}{subsection.3.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.10}Adres\IeC {\'a}\IeC {\v r}ov\IeC {\'a} struktura modulu}{60}{subsection.3.2.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Integrace modulu do informa\IeC {\v c}n\IeC {\'\i }ho syst\IeC {\'e}mu \IeC {\v s}koly}{61}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Integrace modulu}{61}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Nasazen\IeC {\'\i }}{63}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Bezpe\IeC {\v c}nost}{64}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.0.1}XSS}{65}{subsubsection.3.4.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.0.2}CSRF}{65}{subsubsection.3.4.0.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.0.3}Injection}{66}{subsubsection.3.4.0.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Validace formul\IeC {\'a}\IeC {\v r}\IeC {\r u}}{66}{subsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {4}Testov\IeC {\'a}n\IeC {\'\i }}{67}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Jednotkov\IeC {\'e} testy}{67}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Integra\IeC {\v c}n\IeC {\'\i } testy}{68}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Heuristick\IeC {\'a} anal\IeC {\'y}za}{70}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}U\IeC {\v z}ivatelsk\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{71}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}V\IeC {\'y}b\IeC {\v e}r voliteln\IeC {\'y}ch p\IeC {\v r}edm\IeC {\v e}t\IeC {\r u} - studenti}{71}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Vypln\IeC {\v e}n\IeC {\'\i } preferenc\IeC {\'\i } na \IeC {\'u}vazky - u\IeC {\v c}itel\IeC {\'e}}{72}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Z{\' a}v{\v e}r}{75}{chapter*.14}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografie}{77}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{81}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {B}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{83}{appendix.B}
